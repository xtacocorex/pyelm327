from distutils.core import setup

classifiers = ['Development Status :: 3 - Alpha',
               'Operating System :: POSIX :: Linux',
               'License :: OSI Approved :: GNU GPLv2',
               'Intended Audience :: Developers',
               'Programming Language :: Python :: 2.6',
               'Programming Language :: Python :: 2.7',
               'Topic :: Software Development',
               'Topic :: System :: Hardware']

setup(name             = "pyELM327",
      version          = "0.2",
      description      = "Python library for interfacing with ELM-327 based devices",
      long_description = open('README.txt').read(),
      author           = "Robert Wolterman",
      license          = "GNU GPLv2",
      author_email     = "robert.wolterman@gmail.com",
      url              = "https://bitbucket.org/xtacocorex/pyelm327",
      packages         = ["elm327"],
      scripts          = ["samples/elm327unittest.py","samples/dtcgrabber.py"]
) 


