#!/usr/bin/env python

"""
OBD-II LIBRARY
Robert Wolterman (xtacocorex)
https://bitbucket.org/xtacocorex/pyelm327

Python OBD-II Parser

Current PIDS able to be parsed:
Everything from 0x01 to 0x4E with the exceptions of:
 0x01, 0x02, 0x32, 0x41

CHANGELOG:
 09/30/2013 - Formatting cleanup
 08/24/2013 - Added DTC Parsing (Mode 0x03), not tested against an actual vehicle
              reporting valid DTC Codes, also not verified to work if there are
              more than 3 DTC Codes reporting
 08/10/2013 - Initial support for Mode 0x03, fixes for parsing that data 
              since the earlier code assumed there was a PID with the 0x03/0x04 SID
 08/09/2013 - Added support for PIDs 0x01, 0x12, 0x32, 0x41
 07/20/2013 - Initial upload to bitbucket git repo

"""

OBDRespCode = 0x40

DTCHDRMAP = {0x00:"P",0x01:"C",0x02:"B",0x03:"U"}
FUELSYSMAP = {0x01: "ol",0x02: "cl",0x04: "ol-drive",0x08: "ol-fault",0x10: "cl-fault"}
CMDSECAIRSTSMAP = {0x01: "upstream from catalytic",0x02: "downstream from catalytic",0x03:"outside or off"}
O2SENLOC2MAP = {0x01: "o2s11",0x02: "o2s12",0x04: "o2s13",0x08: "o2s14",0x10: "o2s21",0x20: "o2s22",0x40: "o2s23",0x80: "o2s24"}
O2SENLOC4MAP = {0x01: "o2s11",0x02: "o2s12",0x04: "o2s21",0x08: "o2s22",0x10: "o2s31",0x20: "o2s32",0x40: "o2s41",0x80: "o2s42"}
OBDTYPEENUM = {
        0x01: "obd-ii",
        0x02: "obd",
        0x03: "obd + obd-ii",
        0x04: "obd-i",
        0x05: "no obd",
        0x06: "eobd",
        0x07: "eobd + obd-ii",
        0x08: "eobd + obd",
        0x09: "eobd + obd + obd-ii",
        0x0A: "jobd",
        0x0B: "jobd + obd-ii",
        0x0C: "jobd + eobd",
        0x0D: "jobd + eobd + obd-ii",
        0x11: "emd",
        0x12: "emd+",
        0x13: "hd obd-c",
        0x14: "hd pbd",
        0x15: "wwh obd",
        0x17: "hd eobd-i",
        0x18: "hd eobd-i n",
        0x19: "hd eobd-ii",
        0x1A: "hd eobd-ii n",
        0x1C: "obdbr-1",
        0x1D: "obdbr-2"
    }

def supportedpids(x,sid,st):
    spids = []
    # DO THE CONVERSION TO AN INTEGER HERE FOR x
    data = (int(x[0],16) << 24) | (int(x[1],16) << 16) | (int(x[2],16) << 8) | int(x[3],16)
    # WE KNOW THERE ARE 32 PIDS IN A GROUP
    pids = range(st,st+32)
    # LOOP THROUGH PIDS
    for i in xrange(31,-1,-1):
        mask = 1 << i
        cpid = pids[32-i-1]
        if data & mask:
            tmp = "%02x%02x" % (sid,cpid)
            spids.append(tmp)
    
    return ",".join(spids)
    
def bitfieldparse(x,mapdict):
    rstr = ''
    # SPLIT UP DATA INTO BYTES
    for d in x:
        for key in mapdict.keys():
            if (int(d,16) & key) == key:
                rstr += mapdict[key] + ","
    return rstr.rstrip()

def enumparse(x,mapdict):
    return mapdict[x]

def banksensorparse(x):
    a = int(x[0],16)
    b = int(x[1],16)
    volt = a / 200.0
    shrttrmfueltrim = (b - 128) * 100.0 / 128
    return "%4.3f,%5.2f" % (volt,shrttrmfueltrim)

def o2wrlambda1parse(x):
    ab = (int(x[0],16) << 8) | int(x[1],16)
    cd = (int(x[1],16) << 8) | int(x[2],16)
    er = ab / 32768.0
    volt = (cd / 256.0) - 128.0
    return "%4.3f,%4.3f" % (er,volt)

def o2wrlambda2parse(x):
    ab = (int(x[0],16) << 8) | int(x[1],16)
    cd = (int(x[1],16) << 8) | int(x[2],16)
    er = ab * 2.0 / 65535
    volt = cd * 8.0 / 65535
    return "%4.3f,%4.3f" % (er,volt)

def monitorstatusdtcparse(x):
    # RETURN DICTIONARY
    returndict = {
                  "dtc count"     : 0,
                  "mil on"        : False,
                  "ignition type" : "",
                  "tests"         : {
                                     "misfire"     : ["disabled","incomplete"],
                                     "fuel system" : ["disabled","incomplete"],
                                     "components"  : ["disabled","incomplete"],
                                     }
                 }

    # LET US PARSE, X WILL BE A LIST, FIRST CONVERT TO INTEGERS
    x[0] = int(x[0],16)
    x[1] = int(x[1],16)
    x[2] = int(x[2],16)
    x[3] = int(x[3],16)
    # PARSE
    returndict["mil on"]    = bool((x[0] & 0x80) >> 7)
    returndict["dtc count"] = (x[0] & 0x7F)
    if x[1] & 0x08:
        returndict["ignition type"] = "spark"
    else:
        returndict["ignition type"] = "compression"
    # PARSE OUT THE STANDARD TESTS
    if x[1] & 0x01:
        returndict["tests"]["misfire"][0]              = "enabled"
    if x[1] & 0x02:
        returndict["tests"]["fuel system"][0]          = "enabled"
    if x[1] & 0x04:
        returndict["tests"]["components"][0]           = "enabled"
    if x[1] & 0x10:
        returndict["tests"]["misfire"][1]              = "complete"
    if x[1] & 0x20:
        returndict["tests"]["fuel system"][1]          = "complete"
    if x[1] & 0x40:
        returndict["tests"]["components"][1]           = "complete"
    # NOT GOING TO DEAL WITH THE IGNITION MONITORS
    return returndict

def monitorstatusparse(x):
    # RETURN DICTIONARY, LIST IS SET THIS WAY FOR DEFAULTING
    # TO NOT SUPPORT AND HAVE THE TEST BE INCOMPLETE
    returndict     = {"tests"         : {
                      "misfire"              : ["disabled","incomplete"],
                      "fuel system"          : ["disabled","incomplete"],
                      "components"           : ["disabled","incomplete"],
                      "reserved"             : ["disabled","incomplete"],
                      "catalyst"             : ["disabled","incomplete"],
                      "heated catalyst"      : ["disabled","incomplete"],
                      "evaporative system"   : ["disabled","incomplete"],
                      "secondary air system" : ["disabled","incomplete"],
                      "a/c refrigerant"      : ["disabled","incomplete"],
                      "oxygen sensor"        : ["disabled","incomplete"],
                      "oxygen sensor heater" : ["disabled","incomplete"],
                      "egr system"           : ["disabled","incomplete"]
                      }
                      }
    # LET US PARSE, X WILL BE A LIST, FIRST CONVERT TO INTEGERS
    x[0] = int(x[0],16)
    x[1] = int(x[1],16)
    x[2] = int(x[2],16)
    x[3] = int(x[3],16)
    # WE NEED TO NOT USE ELEMENT 0 (A)
    # PARSE B BYTE
    if x[1] & 0x01:
        returndict["tests"]["misfire"][0]              = "enabled"
    if x[1] & 0x02:
        returndict["tests"]["fuel system"][0]          = "enabled"
    if x[1] & 0x04:
        returndict["tests"]["components"][0]           = "enabled"
    if x[1] & 0x08:
        returndict["tests"]["reserved"][0]             = "enabled"
    if x[1] & 0x10:
        returndict["tests"]["misfire"][1]              = "complete"
    if x[1] & 0x20:
        returndict["tests"]["fuel system"][1]          = "complete"
    if x[1] & 0x40:
        returndict["tests"]["components"][1]           = "complete"
    if x[1] & 0x80:
        returndict["tests"]["reserved"][1]             = "complete"
    # PARSE C BYTE
    if x[2] & 0x01:
        returndict["tests"]["catalyst"][0]             = "enabled"
    if x[2] & 0x02:
        returndict["tests"]["heated catalyst"][0]      = "enabled"
    if x[2] & 0x04:
        returndict["tests"]["evaporative system"][0]   = "enabled"
    if x[2] & 0x08:
        returndict["tests"]["secondary air system"][0] = "enabled"
    if x[2] & 0x10:
        returndict["tests"]["a/c refrigerant"][0]      = "enabled"
    if x[2] & 0x20:
        returndict["tests"]["oxygen sensor"][0]        = "enabled"
    if x[2] & 0x40:
        returndict["tests"]["oxygen sensor heater"][0] = "enabled"
    if x[2] & 0x80:
        returndict["tests"]["egr system"][0]           = "enabled"
    # PARSE D BYTE
    if x[3] & 0x01:
        returndict["tests"]["catalyst"][1]             = "complete"
    if x[3] & 0x02:
        returndict["tests"]["heated catalyst"][1]      = "complete"
    if x[3] & 0x04:
        returndict["tests"]["evaporative system"][1]   = "complete"
    if x[3] & 0x08:
        returndict["tests"]["secondary air system"][1] = "complete"
    if x[3] & 0x10:
        returndict["tests"]["a/c refrigerant"][1]      = "complete"
    if x[3] & 0x20:
        returndict["tests"]["oxygen sensor"][1]        = "complete"
    if x[3] & 0x40:
        returndict["tests"]["oxygen sensor heater"][1] = "complete"
    if x[3] & 0x80:
        returndict["tests"]["egr system"][1]           = "complete"
    
    return returndict

def parsedtccodes(x):
    rdict = {"dtc codes" : []}
    
    lx = len(x)
    # LOOP THROUGH LIST OF DATA RETURNED, GO BY 2'S SINCE
    # THE DATA IS BROKEN OUT BY BYTE
    for i in xrange(0,lx,2):
        # COMBINE THE TWO BYTES AS A STRING
        combi = x[i] + x[i+1]
        # DTC CODE STRING CLEARING
        dtcstr = ""
        # LOOP THROUGH EACH "4 BIT" SECTION AND GET THE CODE
        for j in xrange(4):
            # CONVERT OUR "4 BITS" TO AN INTEGER
            tmp = int(combi[j],16)
            # DO WORK
            if j == 0:
                # FIGURE OUT LETTER AND FIRST DECIMAL
                dtcstr += "%s%X" % (DTCHDRMAP[((tmp & 0xC) >> 2)],(tmp & 0x3))
            else:
                dtcstr += "%X" % tmp
    
        # APPEND ONLY IF THE DATA IS NOT P0000,C0000,B0000,U0000
        if dtcstr not in ['P0000','C0000','B0000','U0000']:
            rdict["dtc codes"].append(dtcstr)
    
    return rdict

# TOTALLY PYTHONIC :)
# FORMAT PID:[FUNCT,UNIT,CONVERSION,IMPERIAL UNIT,NAME]
# PULLED FROM WIKIPEDIA: http://en.wikipedia.org/wiki/Table_of_OBD-II_Codes
# VERIFIED THE COMMANDS THAT A 2003 SUBARU OUTBACK WAGON REPORTS ARE ABLE TO 
# BE PARSED WITH THIS
SID0102MAP = {
            # ---------------------------------
            # GROUP 1: 0x01 - 0x20
            0x00:[lambda x,y: supportedpids(x,y,0x01),"",lambda x: x,"","g1_sup_pids"],
            
            # MONITOR STATUS SINCE DTC CLEARED, 4 BYTES
            0x01:[lambda x: monitorstatusdtcparse(x),"",lambda x: x,"","monitor_sts_since_dtc_cleared"],
            #0x02 IS AN INVALID OPTION FOR MODE 01 AND IF ZERO IN MODE 02 TELLS US THAT DATA
            # IS NO GOOD, DON'T SEE THE NEED TO PARSE THIS OUT...
            # FUEL SYSTEM STATUS, 2 BYTES
            0x03:[lambda x: bitfieldparse(x,FUELSYSMAP),"",lambda x: x,"",""],
            # LOAD PERCENTAGE, 1 BYTE
            0x04:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","load_pct"],
            # ENGINE COOLANT TEMPERATURE, 1 BYTE
            0x05:[lambda x: int(x[0],16) - 40,"c",lambda x: (x * 1.8) + 32.0,"f","ect"],
            # FUEL TRIM SHORT TERM BANK 1, 1 BYTE
            0x06:[lambda x: ((int(x[0],16) / 128.0) - 1.0) * 100.0,"%",lambda x: x * 1.0,"%","ftrimstermb1"],
            # FUEL TRIM LONG TERM BANK 1, 1 BYTE
            0x07:[lambda x: ((int(x[0],16) / 128.0) - 1.0) * 100.0,"%",lambda x: x * 1.0,"%","ftrimltermb1"],
            # FUEL TRIM SHORT TERM BANK 2, 1 BYTE
            0x08:[lambda x: ((int(x[0],16) / 128.0) - 1.0) * 100.0,"%",lambda x: x * 1.0,"%","ftrimstermb2"],
            # FUEL TRIM LONG TERM BANK 2, 1 BYTE
            0x09:[lambda x: ((int(x[0],16) / 128.0) - 1.0) * 100.0,"%",lambda x: x * 1.0,"%","ftrimltermb2"],
            # FUEL RAIL PRESSURE, 1 BYTE
            0x0A:[lambda x: int(x[0],16) * 3,"kPa",lambda x: x * 0.1450377,"psi","frp"],
            # MANIFOLD AIR PRESSURE, 1 BYTE
            0x0B:[lambda x: int(x[0],16) * 1,"kPa",lambda x: x * 0.1450377,"psi","map"],
            # RPM, 2 BYTES
            0x0C:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) / 4,"1/min",lambda x: x * 1,"1/min","rpm"],
            # VEHICLE SPEED, 1 BYTE
            0x0D:[lambda x: int(x[0],16) * 1,"km/h",lambda x: x / 1.609344,"mph","vss"],
            # TIMING ADVANCE, 1 BYTE
            0x0E:[lambda x: (int(x[0],16) - 128) * 0.5,"deg",lambda x: x * 1.0,"deg","timing_adv"],
            # INLET AIR TEMPERATURE, 1 BYTE
            0x0F:[lambda x: int(x[0],16) - 40,"c",lambda x: (x * 1.8) + 32.0,"f","iat"],
            # MASS AIR FLOW PRESSURE, 2 BYTES
            0x10:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) / 100.0,"g/s",lambda x: x / 453.59237,"lb/s","maf"],
            # THROTTLE PERCENTAGE, 1 BYTE
            0x11:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","throttle"],
            # COMMANDED SECONDARY AIR STATUS, 1 BYTE
            0x12:[lambda x: bitfieldparse(x,CMDSECAIRSTSMAP),"",lambda x: x, "","cmd_secondary_air_sts"],            
            # O2 SENSOR SUPPORT, 1 BYTE
            0x13:[lambda x: bitfieldparse(x,O2SENLOC2MAP),"",lambda x: x,"","o2sloc"],
            # BANK X SENSOR Y DATA, 2 BYTES EACH RESPONSE (VOLTAGE, PERCENTAGE)
            0x14:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b1s1"],
            0x15:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b1s2"],
            0x16:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b1s3"],
            0x17:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b1s4"],
            0x18:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b2s1"],
            0x19:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b2s2"],
            0x1A:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b2s3"],
            0x1B:[lambda x: banksensorparse(x),"volt,%",lambda x: x,"volt,%","b2s4"],
            # OBD SUPPORT, 1 BYTE
            0x1C:[lambda x: enumparse(int(x[0],16),OBDTYPEENUM),"",lambda x: x,"","obdsup"],
            # O2 SENSOR BANK LOCATION, 1 BYTE
            0x1D:[lambda x: bitfieldparse(x,O2SENLOC4MAP),"",lambda x: x,"","o2sloc"],
            # RUNTIME, 2 BYTES
            0x1F:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 1.0,"s",lambda x: x / 60.0,"min","runtime"],
            # GROUP 2 SUPPORTED PIDS, 4 BYTES
            0x20:[lambda x,y: supportedpids(x,y,0x21),"",lambda x: x,"","g2_sup_pids"],
            
            # ---------------------------------
            # GROUP 2: 0x21 - 0x40
            
            # DISTANCE TRAVELLED WITH MIL ON, 2 BYTES
            0x21:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 1.0,"km",lambda x: x * 0.621371,"mi","mil_on_dist"],
            # FUEL RAIL PRESSURE (REL VACUUM), 2 BYTES
            0x22:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 0.079,"kPa",lambda x: x * 0.1450377,"psi","frp_rel_vac"],
            # FUEL RAIL PRESSURE (DIESEL/GAS DIRECT), 2 BYTES
            0x23:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 10.0,"kPa",lambda x: x * 0.1450377,"psi","frp_dies_gas_direct"],
            # O2 SENSOR WR LAMBDA #1 , 4 BYTES EACH RESPONSE
            0x24:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s1wrlam1"],
            0x25:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s2wrlam1"],
            0x26:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s3wrlam1"],
            0x27:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s4wrlam1"],
            0x28:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s5wrlam1"],
            0x29:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s6wrlam1"],
            0x2A:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s7wrlam1"],
            0x2B:[lambda x: o2wrlambda1parse(x),"-,volt",lambda x: x,"-,volt","o2s8wrlam1"],
            # COMMANDED EGR PERCENTAGE, 1 BYTE
            0x2C:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","cmd_egr"],
            # EGR ERROR, 1 BYTE
            0x2D:[lambda x: (int(x[0],16) / 128.0) * 100.0/128.0,"%",lambda x: x * 1.0,"%","egr_err"],
            # COMMANDED EVAP PURGE PERCENTAGE, 1 BYTE
            0x2E:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","cmd_evap_purge"],
            # FUEL LEVEL PERCENTAGE, 1 BYTE
            0x2F:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","fuel_level"],
            # # WARMUPS SINCE DTC CLEARED, 1 BYTE
            0x30:[lambda x: int(x[0],16),"",lambda x: x,"","num_warmups_since_dtc_clear"],
            # DISTANCE TRAVELLED SINCE DTC CLEARED, 2 BYTES
            0x31:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 1.0,"km",lambda x: x * 0.621371,"mi","mil_cleared_dist"],
            # EVAP SYSTEM VAPOR PRESSURE, 2 BYTES (TWO'S COMPLIMENT)
            0x32:[lambda x: (((int(x[0],16) << 8) | int(x[1],16)) / 4) - 8192,"pa",lambda x : x * 0.000145037738,"psi","evap_sys_vapor_pressure"],
            # BAROMETRIC PRESSURE
            0x33:[lambda x: int(x[0],16) * 1.0,"kPa",lambda x: x * 0.1450377,"psi","baro"],
            # O2 SENSOR WR LAMBDA #2, 4 BYTES EACH RESPONSE
            0x34:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s1wrlam2"],
            0x35:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s2wrlam2"],
            0x36:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s3wrlam2"],
            0x37:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s4wrlam2"],
            0x38:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s5wrlam2"],
            0x39:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s6wrlam2"],
            0x3A:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s7wrlam2"],
            0x3B:[lambda x: o2wrlambda2parse(x),"-,amp",lambda x: x,"-,amp","o2s8wrlam2"],
            # CATALYST TEMPERATURE
            0x3C:[lambda x: (((int(x[0],16) << 8) | int(x[1],16)) / 10.0) - 40.0,"c",lambda x: (x * 1.8) + 32.0,"f","b1s1cattemp"],
            0x3D:[lambda x: (((int(x[0],16) << 8) | int(x[1],16)) / 10.0) - 40.0,"c",lambda x: (x * 1.8) + 32.0,"f","b2s1cattemp"],
            0x3E:[lambda x: (((int(x[0],16) << 8) | int(x[1],16)) / 10.0) - 40.0,"c",lambda x: (x * 1.8) + 32.0,"f","b1s2cattemp"],
            0x3F:[lambda x: (((int(x[0],16) << 8) | int(x[1],16)) / 10.0) - 40.0,"c",lambda x: (x * 1.8) + 32.0,"f","b2s2cattemp"],
            # GROUP 3 SUPPORTED PIDS, 4 BYTES
            0x40:[lambda x,y: supportedpids(x,y,0x41),"",lambda x: x,"","g3_sup_pids"],

            # ---------------------------------
            # GROUP 3: 0x41 - 0x60
            
            # MONITOR STATUS DURING THIS DRIVE CYCLE
            0x41:[lambda x: monitorstatusparse(x),"-",lambda x: x,"-","monitor_sts_drive_cycle"],
            # CONTROL MODULE VOLTAGE, 2 BYTES
            0x42:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) / 1000,"volt",lambda x: x,"volt","ctrl_module_voltage"],
            # ABSOLUTE LOAD VALUE, 2 BYTES
            0x43:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 100.0 / 255.0,"%",lambda x: x,"%","abs_load_value"],
            # COMMAND EQUIVALENCE RATIO
            0x44:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) / 32768.0,"-",lambda x: x,"-","cmd_equivalence_ratio"],
            # RELATIVE THROTTLE POSITION, 1 BYTE
            0x45:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","rel_throttle_pos"],
            # AMBIENT AIR TEMPERATURE, 1 BYTE
            0x46:[lambda x: int(x[0],16) - 40,"c",lambda x: (x * 1.8) + 32.0,"f","ambient_air"],
            # ABSOLUTE THROTTLE POSITION, 1 BYTE EACH
            0x47:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","abs_throttle_b"],
            0x48:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","abs_throttle_c"],
            0x49:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","abs_throttle_d"],
            0x4A:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","abs_throttle_e"],
            0x4B:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","abs_throttle_f"],
            # COMMANDED THROTTLE ACTUATOR
            0x4C:[lambda x: int(x[0],16) * 100.0 / 255.0,"%",lambda x: x * 1.0,"%","cmd_throttle_actuator"],
            # TIME WITH MIL ON, 2 BYTES
            0x4D:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 1,"min",lambda x: x,"min","miltime"],
            # TIME SINCE DTC CLEAR, 2 BYTES
            0x4E:[lambda x: ((int(x[0],16) << 8) | int(x[1],16)) * 1,"min",lambda x: x,"min","cleardtctime"],
}

def obdparser(rsid,rpid,rdatl,unittype):
    # FUNCTION VARIABLES
    resp  = ""
    unit  = ""
    field = ""
    
    # CHECK THE SID TO SEE WHAT TO DO
    if rsid == 0x01 or rsid == 0x02:
        # USE THE SID0102MAP DICTIONARY
        if rsid == 0x01 and rpid == 0x02:
            resp == "invalid command"
        elif rsid == 0x02 and rpid == 0x01:
            resp == "invalid command"
        elif rpid not in SID0102MAP.keys():
            resp = "pid %02X unavailable" % rpid
        else:
            # DO WORK DEPENDENT UPON UNIT TYPE
            if unittype == "imperial":
                # CHECK TO SEE IF WE ARE A GROUP SUPPORT PID
                # THIS FUNCTION HAS 2 ARGUMENTS
                # rpid CAN ALSO BE: 0x60, 0x80, 0xA0, 0xC0, 0xE0
                # BUT THESE ARE NOT IMPLEMENTED
                if rpid in [0x00,0x20,0x40]:
                    resp  = SID0102MAP[rpid][2](SID0102MAP[rpid][0](rdatl,rsid))
                else:
                    resp  = SID0102MAP[rpid][2](SID0102MAP[rpid][0](rdatl))
                unit  = SID0102MAP[rpid][3]
                field = SID0102MAP[rpid][4]
            else:
                # CHECK TO SEE IF WE ARE A GROUP SUPPORT PID
                # THIS FUNCTION HAS 2 ARGUMENTS
                if rpid in [0x00,0x20,0x40]:
                    resp  = SID0102MAP[rpid][0](rdatl,rsid)
                else:
                    resp  = SID0102MAP[rpid][0](rdatl)
                unit  = SID0102MAP[rpid][1]
                field = SID0102MAP[rpid][2]
    elif rsid == 0x03 or rsid == 0x07:
        # BREAK APART THE RESPONSE, THERE IS NO PID
        resp = parsedtccodes(rdatl)
        unit = "-"
        field = "dtc_codes"
    elif rsid == 0x04:
        resp = "-"
        unit = ""
        field = "dtc_clear_cmd"
    elif rsid in [0x05,0x06,0x07,0x08,0x0A]:
        # NOT IMPLEMENTED
        resp = "sid %02x not implemented" % rsid
    elif rsid == 0x09:
        # SUPPORTED PIDS
        if rpid == 0x00:
            resp = supportedpids(rdatl,rsid,0x01)
            unit = "-"
            field = "sid_09_supported_pids"
        else:
            # CALIBRATION STUFF
            print "in obd_ii.obdparser(), printing rdatl"
            print rdatl
    else:
        # INVALID
        resp = "sid %02x invalid" % rsid

    # RETURN OUR DATA AS A TUPLE
    return (resp,unit,field)

