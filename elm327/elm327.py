#!/usr/bin/env python

"""
ELM-327 DEVICE
Robert Wolterman (xtacocorex)
https://bitbucket.org/xtacocorex/pyelm327

Python ELM-327 Device "Driver"
Tested against both ELM-327 and STN1110 devices

CHANGELOG:
 06/25/2015 - Added support for calculating MPG from Vehicle Speed and MAF
 12/19/2013 - Changed around the response() function to look for errors
              Updated all calls to response() to grab the tuple its returning
              Changed out the retry for the OBD commands to retry on any Error
              Added thread status updates when receiving a response so user
              tell if something is wrong
              Added getprotocollongname() function to return a more lengthy
              protocol name
              General code formatting things because PyCharm keeps telling me
              that they go against the PEP...
 11/28/2013 - More fixes to the capturing of the <DATA ERROR and recovering
              Added status variable to the device code to allow code using
              this library to know what is going on when running this as a
              separate thread
 11/27/2013 - Fixes to the unit test code, adding retry capability when the
              ELM-327 device returns <DATA ERROR or <RX ERROR
 11/10/2013 - Implementing a threading approach to allow this class
              to run on its own
 09/30/2013 - Formatting cleanup
 08/31/2013 - Fixed bug for determing what to grab for the OBD parser when commanding
              mode 0x03 and 0x04
 08/24/2013 - Fixed duplicate Checksum key in the OBD Data return dictionary
              Added support for the following commands: ATM, ATSS, ATR, ATPC
 08/10/2013 - Fixed an issue where when commanding Mode 0x03 and 0x04, the parser
              would extract a PID out of the return data which is incorrect
 07/20/2013 - Initial upload to bitbucket git repo

"""

# MODULE IMPORTS
import sys
import threading
import time
import Queue
import obdii_lib
try:
    import serial
except ModuleError:
    sys.stderr.write("pyserial is needed")
    sys.exit()

# EXCEPTIONS
ELM327CommandInvalid = ValueError('Invalid Command sent to ELM-327 Device')
ELM327NotCreated = ValueError('ELM-327 Device has not been created')

# ELM-327 PROTOCOL MAPPING
ELM327Protocols = {
    0: "Automatic",
    1: "SAE J1850 PWM (41.6 kbaud)",
    2: "SAE J1850 VPW (10.4 kbaud)",
    3: "ISO 9141-2 (5 baud init, 10.4 kbaud)",
    4: "ISO 14230-4 KWP (5 baud init, 10.4 kbaud)",
    5: "ISO 14230-4 KWP (fast init, 10.4 kbaud)",
    6: "ISO 15765-4 CAN (11 bit ID, 500 kbaud)",
    7: "ISO 15765-4 CAN (29 bit ID, 500 kbaud)",
    8: "ISO 15765-4 CAN (11 bit ID, 250 kbaud)",
    9: "ISO 15765-4 CAN (29 bit ID, 250 kbaud)",
    10: "SAE J1939 CAN (29 bit ID, 250 kbaud)",
    11: "USER1 CAN",
    12: "USER2 CAN"
}


class ELM327(threading.Thread):
    """
     ELM-327 OBD-II DEVICE
    """
    def __init__(self,
                 comport,
                 baudrate=38400,
                 bytesize=serial.EIGHTBITS,
                 parity=serial.PARITY_NONE,
                 stopbits=serial.STOPBITS_ONE,
                 timeout=2,
                 units="imperial"):
        """
        __init__:
        Class Constructor for the ELM-327 device
        
        Currently Supported AT Commands:
        ATZ
        ATD
        ATWS
        ATI
        ATE
        ATL
        ATH
        ATSP00
        ATSP
        ATSPA
        ATDP
        ATDPN
        ATM
        ATSS
        ATR
        ATPC
        
        Input:
            comport  - The name of the serial port to use
            baudrate - Baud Rate of the device, defaults to 38400 per ELM-327 Documentation
            bytesize - Serial Config for the number of bits in a byte, defaults to serial.EIGHTBITS
            parity   - Serial Config for parity, defaults to serial.PARITY_NONE
            stopbits - Serial Config for the stop bits, defaults to serial.STOPBITS_ONE
            timeout  - Timeout for the serial port in milliseconds, defaults to 2
            units    - Unit type for the OBD-II parser, defaults to imperial
        Output:
            None
        """
        # INIT THE INHERITED CLASS
        threading.Thread.__init__(self)
        # DEFINE OUR SERIAL PORT
        self.port = serial.Serial(comport, baudrate, bytesize, parity, stopbits, timeout)
        self.name = comport
        self.unittype = units
        self.endline = "\r"
        # ELM-327 DEVICE VARIABLES
        self.myaddr = 0x41     # DEFAULT OBD ADDRESS
        self.id = ""
        self.protocol = ""
        self.echomode = False
        self.linefeed = False
        self.headerson = False
        self.connected = False
        self.memoryenable = False
        self.responsesenable = False
        self.elmconnected = False
        self.vehicleconnected = False
        # THREADED RUN VARIABLES
        self.obdcmdlist = []
        self.obdrespqueuelist = []
        self.status = 'not opened'
        self.intercmddelay = 0.2
        self.loopdelay = 0.5
        self.dead = False
        # OTHER VARIABLES
        self.wantmpg = False
        self.candompg = False
        self.mpgqueue = Queue.Queue(1)
        
    def open(self):
        """
        open:
        Opens the serial connection to the ELM-327 device
        Input:
            None
        Output:
            None
        """
        self.port.close()
        self.port.open()
        self.elmconnected = True
        self.status = 'connection opened'

    def kill(self):
        """
        kill:
        Kills the loop when running the thread
        Input:
            None
        Output:
            None
        """
        self.dead = True

    def close(self):
        """
        close:
        Closes the serial connection to the ELM-327 device
        Input:
            None
        Output:
            None
        """
        self.port.close()
        self.elmconnected = False
        self.status = 'connection closed'

    def getstatus(self):
        """
        getstatus:
        Gets the status of the ELM-327 device
        Input:
            None
        Output:
            status - ELM-327 device status
        """
        return self.status

    def setunits(self, unittype):
        """
        setunits:
        Sets the unit type for the OBD-II parser
        Input:
            unittype - String to specify the unit type: metric or imperial
        Output:
            None
        """
        if unittype not in ["metric", "imperial"]:
            raise ValueError("Improper Unit Specified")
        else:
            self.unittype = unittype

    def getunits(self):
        """
        getunits:
        Gets the unit type for the OBD-II parser
        Input:
            None
        Output:
            unittype - OBD-II Unit type, class defaults to imperial
        """
        return self.unittype

    def setcalcmpg(self, wantmpg):
        """
        setcalcmpg:
        Tells this code we want MPG data
        Input:
            wantmpg - boolean
        Output:
            None
        """
        self.wantmpg = wantmpg

    def command(self, cmdstr):
        """
        command:
        Tells the ELM-327 to do something
        Input:
            cmdstr - String of command to run *without* the endline character
        Output:
            None
        """
        self.port.flushInput()
        self.port.flushOutput()
        self.port.write(cmdstr+self.endline)

    def response(self, endstr=">"):
        """
        response:
        Gets the response from the previous command sent to the ELM-327 device
        Input:
            endstr - String to look for that defines the end of the response
                     defaults to >
        Output:
            List of data in the following format, this is done to support multi-line
            responses, something the other Python OBD Libraries do not do well or at all
            properly
            ['command','data frame 1', 'data frame 2','...','data frame n']
            Followed by an identifier field:
            0 = Good
            1 = Uknown Command (received a ?)
            2 = Stopped
            3 = Unable to Connect
            4 = DATA ERROR, <DATA ERROR, or <RX ERROR - this is one we want for retries
            5 = No Data
            6 = Bus or CAN Error
            7 = Buffer Full
            8 = Bus Busy
        Notes:
            The ELM-327 will return the following for a single line response
            Command Sent\rData Frame 1\r>\r
            The ELM-327 will return the following for a multi-line response
            Command Sent\rData Frame 1\rData Frame 2\rData Frame n\r>\r
        """
        buffer = ""
        tmp = self.port.read(1)
        while tmp != endstr:
            # PER NOTE ON PAGE 6 OF ELM-327 DATASHEET, SKIP OVER NULL
            if tmp is not None:
                buffer += tmp
            tmp = self.port.read(1)
        buffer = buffer.rstrip().split("\r")

        # DEBUG
        #print buffer

        # DETERMINE IDENTITY OF DATA BY CHECKING FOR ERROR CONDITIONS
        # SINCE BUFFER IS NOW SPLIT INTO A LIST, WE KNOW THAT WE CAN'T
        # USE ITEM 0 SINCE IT CONTAINS ONLY THE COMMAND
        # WE NEED TO LOOK AT ITEM 1 FOR EVERYTHING DUE TO THE ERROR DATA
        # BEING PUT IN THE FIRST DATA FRAME FOLLOWING SID AND PID

        # WHILE HERE, ALSO SET THE THREAD STATUS TO LET ANYONE MONITORING
        # THAT SOMETHING POSSIBLY CHANGED
        # TODO: ADD REST OF ERROR CONDITIONS PER DATASHEET AS NEEDED
        identifier = 0
        if "?" in buffer[1]:
            identifier = 1
            self.status = 'unknown command'
        elif "STOPPED" in buffer[1]:
            identifier = 2
            self.status = 'protocol stopped'
        elif "UNABLE TO CONNECT" in buffer[1]:
            identifier = 3
            self.status = 'unable to connect'
        # COVER BOTH DATA ERROR CONDITIONS WITH ONE CHECK :)
        elif "DATA ERROR" in buffer[1] or "<RX ERROR" in buffer[1]:
            identifier = 4
            self.status = 'data or rx error'
        elif "NO DATA" in buffer[1]:
            identifier = 5
            self.status = 'no data received'
        elif "BUS ERROR" in buffer[1] or "CAN ERROR" in buffer[1]:
            identifier = 6
            self.status = 'bus/can error'
        elif "BUFFER FULL" in buffer[1]:
            identifier = 7
            self.status = 'buffer full'
        elif "BUS BUSY" in buffer[1]:
            identifier = 8
            self.status = 'bus busy'
        else:
            # KEEP IDENTIFIER AS 0
            identifier = 0
            self.status = 'running'

        # DEBUG
        #print buffer, identifier

        # RETURN DATA WITH OUR IDENTIFIER FLAG
        return buffer, identifier

    def reset(self, type):
        """
        reset:
        Tells the ELM-327 to reset
        Input:
            type - String to determine the type of reset:
               "all"      - Resets the entire device (ATZ)
               "settings" - Resets just the settings (ATD)
               "software" - Software reset (ATWS)
        Output:
            None
        """
        # VERIFY TYPE AND COMMAND APPROPRIATELY
        if type == "all":
            self.command("ATZ")
        elif type == "settings":
            self.command("ATD")
        elif type == "software":
            self.command("ATWS")
        else:
            raise ValueError("Invalid Reset Type Specified")
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()

    def identify(self):
        """
        identify:
        Get the device information from the ELM-327 device
        Input:
            None
        Output:
            id - String of ELM-327 device identity
        """
        self.command("ATI")
        # READ THE RESPONSE AND SAVE
        self.id, idt = self.response()
        return self.id

    def setecho(self, mode):
        """
        setecho:
        Tells the ELM-327 and this class if we want the device to echo all commands
        Input:
            mode - Boolean to enable/disable command echo
        Output:
            None
        """
        self.echomode = mode
        cmd = "ATE%d" % self.echomode
        self.command(cmd)
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)

    def setmemoryenable(self, mode):
        """
        setmemoryenable:
        Tells the ELM-327 and this class if we want the device store memory
        Input:
            mode - Boolean to enable/disable memory
        Output:
            None
        """
        self.memoryenable = mode
        cmd = "ATM%d" % self.memoryenable
        self.command(cmd)
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)

    def setresponses(self, mode):
        """
        setresponses:
        Tells the ELM-327 and this class that we want to enable or disable responses
        Input:
            mode - String for your line feed of choice
        Output:
            None
        """
        self.responsesenable = mode
        cmd = "ATR%d" % self.responsesenable
        self.command(cmd)
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)

    def setlinefeed(self, mode):
        """
        setlinefeed:
        Tells the ELM-327 and this class that we want to change the line feed character
        that is returned from the ELM-327 device
        Input:
            mode - String for your line feed of choice
        Output:
            None
        """
        self.linefeed = mode
        cmd = "ATL%d" % self.linefeed
        self.command(cmd)
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)

    def setheaderdisplay(self, mode):
        """
        setheaderdisplay:
        Tells the ELM-327 and this class if we want to display the OBD-II headers
        Header display is disabled by default
        Input:
            mode - Boolean to enable/disable header display
        Output:
            None
        """
        self.headerson = mode
        cmd = "ATH%d" % self.headerson
        self.command(cmd)
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)

    def setstandardsearchorder(self):
        """
        setstandardsearchorder:
        Sets the standard search order for the ELM-327 device
        Input:
            None
        Output:
            None
        """
        self.command("ATSS")
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid("ATSS")

    def setprotocolonlyauto(self):
        """
        setprotocolonlyauto:
        Sets the current protocol to be auto mode only
        Input:
            None
        Output:
            None
        """
        self.command("ATSP00")
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid("ATSP00")

    def setprotocol(self, protocol):
        """
        setprotocol:
        Sets the current protocol to the protocol number without auto mode
        Input:
            protocol - OBD-II protocol as a number
        Output:
            None
        """
        cmd = "ATSP%d" % protocol
        self.command(cmd)
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)

    def setprotocolauto(self, protocol):
        """
        setprotocolauto:
        Sets the current protocol to the protocol number with auto mode
        Input:
            protocol - OBD-II protocol as a number
        Output:
            None
        """
        cmd = "ATSPA%d" % protocol
        self.command(cmd)
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)

    def getprotocol(self):
        """
        getprotocol:
        Returns the current protocol set in the ELM-327 device
        Input:
            None
        Output:
            Current ELM-327 Protocol as a string
        """
        self.command("ATDP")
        # READ THE RESPONSE AND RETURN
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)
        else:
            return tmp[1]

    def getprotocollongname(self):
        """
        getprotocollongname:
        Returns the current protocol long name as it is set in the ELM-327 device
        Input:
            None
        Output:
            Current ELM-327 Protocol Long name from the ELM327Protocols Dictionary
        """
        cmd = "ATDPN"
        self.command(cmd)
        # READ THE RESPONSE AND RETURN
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)
        else:
            return ELM327Protocols[int(tmp[1])]

    def getprotocolnumber(self):
        """
        getprotocolnumber:
        Returns the current protocol number set in the ELM-327 device
        Input:
            None
        Output:
            Current ELM-327 Protocol as a number, refer to the ELM327Protocols
            dictionary above for a mapping
        """
        cmd = "ATDPN"
        self.command(cmd)
        # READ THE RESPONSE AND RETURN
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid(cmd)
        else:
            return tmp[1]
    
    def closeprotocol(self):
        """
        closeprotocol:
        Closes the current running protocol on the ELM-327 device
        Input:
            None
        Output:
            None
        """
        self.command("ATPC")
        # READ THE RESPONSE AND DUMP IT
        tmp, idt = self.response()
        if idt == 1:
            raise ELM327CommandInvalid("ATPC")
        
    # CONNECT TO VEHICLE
    def connect(self, numretries=3, protocol=None, protocolauto=True):
        """
        connect:
        Connects to the vehicle, sends 0x01 and 0x00 as the SID/PID combo since all 
        vehicles are required to support this
        
        Inputs:
            numretries   - Number of tries to attempt before exiting the function
            protocol     - OBD-II Protocol to use
            protocolauto - Boolean to determine if, when setting a protocol, to use 
                           setprotocol or setprotocolauto,
                           This defaults to true on the off chance one picks the wrong
                           protocol, this will tell the ELM-327 to try other protocols
        Outputs:
            attemptcount - Number of attempts it took to connect to the vehicle
            status       - String of the current status (not connected, connected)
        """
        # TODO: THINK ABOUT CHANGING THIS FUNCTION TO VALIDATE CONNECTION
        #       WITH THE STATUS STRINGS INSTEAD OF LOOKING AT THE OBD RESPONSE
        #       NOT SURE WHICH IS THE BEST SOLUTION
        cstatus = "not connected"
        # SET PROTOCOL IS VALUE IS NOT NONE
        if protocol is not None:
            if protocolauto:
                self.setprotocolauto(protocol)
            else:
                self.setprotocol(protocol)
        # LOOP THROUGH OUR RETRIES, EXIT IF WE SUCCEED
        attemptcount = 0
        while attemptcount < numretries:
            # SEND THE COMMAND INTO THE DEVICE
            mycmd = "%02x%02x" % (0x01, 0x00)
            self.command(mycmd)
            # INCREMENT BIG COUNTER
            attemptcount += 1
            #print attemptcount, numretries
            # GET RESPONSE FROM VEHICLE
            resp, idt = self.response()
            #print "tmp (response) in elm327.sendobd()"
            #print resp
            # FIGURE OUT STATUS
            if "41 00" == resp[-1][0:5]:
                # WE HAVE AN ACTUAL OBD RESPONSE
                # BREAK OUT OF THE WHILE LOOP
                # WE DON'T CARE ABOUT PARSING THE DATA
                # SINCE THE INTENT OF THIS FUNCTION
                # IS TO FIND/PROVE OUT THE PROTOCOL
                cstatus = "connected"
                self.status = 'connected to vehicle'
                self.vehicleconnected = True
                break
            else:
                #print resp
                # NOTHING GOOD WAS RETURNED IN
                # "UNABLE TO CONNECT" in resp[-1]
                # "ERROR" in resp[-1]
                pass
        # RETURN THE NUMBER OF ATTEMPTS
        return attemptcount, cstatus

    # OBD-II REQUEST/RESPONSE
    def sendobd(self, sid, pid=None, setheader=False, headerinfo=None):
        """
        sendobd:
        Will send in the OBD command to the ELM-327 Device and returns the 
        appropriate data
        Inputs:
            sid        - SID
            pid        - PID (defaults to none (SID 03 and 04 don't require PID)
            setheader  - Do we need to set the header
            headerinfo - What we need to set the header to
        Output:
            rdict with the following format
            {"name"     : "",
             "priority" : "",
             "target"   : "",
             "source"   : "",
             "sid"      :  0,
             "pid"      :  0,
             "response" : "",
             "unit"     : "",
             "checksum" : ""}
        """
        # SETUP rdict
        rdict = {"name": "-",
                 "priority": "-",
                 "target": "-",
                 "source": "-",
                 "sid": 0,
                 "pid": 0,
                 "response": "-",
                 "unit": "-",
                 "checksum": "-"
                 }
        # CONVERT SID AND PID FROM HEX TO STRING
        nsid = "%02X" % sid
        mycmd = nsid
        if pid is not None:
            npid = "%02X" % pid
            mycmd += npid
        # SET THE HEADER
        # TODO: ADD THE ABILITY TO SET THE HEADER FIELDS
        if setheader:
            print "NOT IMPLEMENTED: set header in sendobd"

        # SEND DATA AND WAIT FOR RESPONSE
        # RETRY IF WE GET AN ERROR FROM THE DEVICE
        numretries = 2
        retrycount = 0
        goodresponse = False
        while retrycount <= numretries:

            # SEND OUT SID + PID
            self.command(mycmd)
            # IMMEDIATELY READ THE RESPONSE
            resp, identitier = self.response()
            # CHECK TO SEE IF WE GOT AN ERROR SO WE
            # CAN RETRY
            if identitier == 0:
                goodresponse = True
                break
            else:
                retrycount += 1

        #print "DEBUG", resp
        # PROCEED IF WE HAVE A GOOD RESPONSE
        # IF NOT, RETURN A BLANK RESPONSE DICTIONARY
        if goodresponse:
            # DUMP THE FIRST ELEMENT IN THE LIST AS IT IS THE COMMAND WE SENT IN
            obdresp = resp[1:]
            numframes = len(obdresp)

            # CHECK FOR OTHER ERRORS
            # TODO: CLEAN THIS IF STATEMENT UP AS WE SHOULDN'T GET HERE IF DATA IS BAD
            # FROM THE RESPONSE
            if "NO DATA" in obdresp[0]:
                rdict["sid"] = sid   # INPUT SID
                rdict["pid"] = pid   # INPUT PIDs
                rdict["response"] = "no data"
            else:
                tmp = obdresp[0].split(" ")
                # EXTRACT DATA
                if self.headerson:
                    ldat = len(tmp)
                    rdict["priority"] = tmp[0]
                    rdict["target"] = tmp[1]
                    rdict["source"] = tmp[2]
                    rdict["checksum"] = tmp[ldat-1]
                    # SID/PID - INTEGERS BECAUSE IT'S EASIER TO SUPPORT
                    # CLEARING OUT THE RESPONSE CODE FROM THE SID RESPONSE
                    rsid = int(tmp[3], 16)
                    rdict["sid"] = rsid - (rsid & obdii_lib.OBDRespCode)
                    rdict["pid"] = int(tmp[4], 16)
                    # KEEPING AS STRINGS, LETTING PARSER TURN TO INTEGERS
                    rdatl = tmp[5:ldat-2]
                else:
                    # SID/PID - INTEGERS BECAUSE IT'S EASIER TO SUPPORT
                    # CLEARING OUT THE RESPONSE CODE FROM THE SID RESPONSE
                    rsid = int(tmp[0], 16)
                    rdict["sid"] = rsid - (rsid & obdii_lib.OBDRespCode)
                    # FIGURE OUT IF WE ARE USING A SID THAT DOESN'T REQUIRE A PID
                    # 0x03 AND 0x04
                    if rdict["sid"] in [0x03, 0x04]:
                        rdict["pid"] = 0  # DEFAULT TO 0
                        # KEEPING AS STRINGS, LETTING PARSER TURN TO INTEGERS
                        # NEED TO NOT GRAB A PID VALUE
                        rdatl = tmp[1:]
                    else:
                        rdict["pid"] = int(tmp[1], 16)
                        # KEEPING AS STRINGS, LETTING PARSER TURN TO INTEGERS
                        rdatl = tmp[2:]
                # IF NUMFRAMES IS GREATER THAN 1 APPEND THE REST OF THE DATA TO rdatl
                if numframes > 1:
                    for i in xrange(1, numframes):
                        rdatl += obdresp[i].split(" ")
                # DON'T USE rdat HERE BECAUSE IT MAKES IT DIFFICULT TO PARSE
                rdict["response"], rdict["unit"], rdict["name"] = obdii_lib.obdparser(rdict["sid"], rdict["pid"], rdatl, self.unittype)
        else:
            rdict["sid"] = sid   # INPUT SID
            rdict["pid"] = pid   # INPUT PIDs
            rdict["response"] = "no data"

        return rdict

    def calcmpg(self,reqdata,vssdata,mafdata):
        """
        calcmpg:
        Calculate the fuel efficiency using speed and mass air flow
        For use by the thread only
        Input:
            reqdata - boolean if the data needs to be grabbed before calculating 
            vssdata - dictionary containing speed data
            mafdata - dictionary containing maf data
        Output:
            rdict - dictionary containing mpg data
        """
        # SETUP rdict
        rdict = {"name": "mpg",
                 "priority": "-",
                 "target": "-",
                 "source": "-",
                 "sid": -1,
                 "pid": -1,
                 "response": "-",
                 "unit": "-",
                 "checksum": "-"
                 }
        # REQUEST THE DATA FROM THE VEHICLE
        if reqdata:
            vssdata = self.sendobd(0x01,0x0D)
            mafdata = self.sendobd(0x01,0x10)
        # PULL THE DATA OUT OF THE DICTIONARY
        vss = vssdata['response']
        maf = mafdata['response']
        # CREATE THE MPG DATA
        if self.unittype == "metric":
            # SET UNIT
            rdict['unit'] = "kmpl"
            # 14.7 (AIR/FUEL RATIO) * 770g/L (DENSITY OF GAS) * kmh / (SECONDS PER HOUR * g/s)
            mpg = (14.7 * 770.0 * vss) / (3600.0 * maf )
        else:
            rdict['unit'] = "mpg"
            # 14.7 * 6.17 lb/gal * mph / (SECONDS PER HOUR * lb/s)
            mpg = (14.7 * 6.17 * vss) / (3600 * maf)
        rdict['response'] = mpg
        return rdict

    # SET THREAD RUN PARAMETERS
    def setuplooprun(self,cmdlist,intercmddelay,loopdelay,numretries=3,protocol=None,protocolauto=True):
        """
        setuplooprun:
        Will set the variables needed for the run loop to perform its work,
        this will be obdii commands (sid,pid) and a delay between device
        access
        Inputs:
            cmdlist - list of dictionary with sid pid values [{'sid':0x01,'pid':0x0C}]
            intercmddelay - delay between command
            loopdelay - delay following commands before starting requesting data
        Outputs:
            None, but the responses are put into separate queues of 1 element each
        """
        self.obdcmdlist = cmdlist
        self.intercmddelay = intercmddelay
        self.loopdelay = loopdelay
        self.connnumretries = numretries
        self.initprotocol = protocol
        self.initprotocolauto = protocolauto
        # CREATE QUEUE LIST
        for i in xrange(len(self.obdcmdlist)):
            self.obdrespqueuelist.append(Queue.Queue(1))
        # CHECK TO SEE IF WE HAVE THE REQUIRED PIDS FOR MPG (VSS (0x0D) AND MAF (0x10))
        havevss = False
        havemaf = False
        for cmd in self.obdcmdlist:
            if cmd['pid'] == 0x0D:
                havevss = True
            elif cmd['pid'] == 0x10:
                havemaf = True
        if havevss and havemaf:
            self.candompg = True

    # RUN
    def run(self):
        """
        run:
        Overloaded function from threading.Thread
        """
        # CHECK TO SEE IF THE ELM DEVICE IS CONNECTED
        if not self.elmconnected:
            self.open()

        # CHECK TO SEE IF WE'RE CONNECTED TO THE VEHICLE
        if not self.vehicleconnected:
            self.connect(self.connnumretries, self.initprotocol, self.initprotocolauto)

        # RUN OUR LOOP
        self.status = 'running'
        tmpvssdata = ''
        tmpmafdata = ''
        while not self.dead:
            # LOOP THROUGH OUR COMMANDS
            for i in xrange(len(self.obdcmdlist)):
                # SLEEP THE INTER COMMAND DELAY
                time.sleep(self.intercmddelay)
                # SEND THE COMMAND
                resp = self.sendobd(self.obdcmdlist[i]['sid'], self.obdcmdlist[i]['pid'])
                # STORE OFF THE DATA FOR VSS AND MAF IF WE HAVE THEM
                if self.cmdlisthavereqpidsformpg:
                    if self.obdcmdlist[i]['pid'] == 0x0D:
                        tmpvddsata = resp
                    elif self.obdcmdlist[i]['pid'] = 0x10:
                        tmpmafdata = resp
                # DEBUG
                #resp = self.obdcmdlist[i]
                # ADD THE RESPONSE TO THE QUEUE
                if not self.obdrespqueuelist[i].full():
                    self.obdrespqueuelist[i].put(resp)
                
                # CALCULATE MPG IF WE HAVE THE DATA REQUIRED AND WE WANT IT
                if self.wantmpg and self.candompg:
                    resp = self.calcmpg(False,tmpvssdata,tmpmafdata)
                    if not self.mpgqueue.full():
                        self.mpgqueue.put(resp)

        # CLOSE CONNECTION
        self.status = 'stopped'
        self.close()

        # CLEAR QUEUES
        for rqueue in self.obdrespqueuelist:
            rqueue.get()
        
        if self.wantmpg:
            self.mpgqueue.get()


# UNIT TEST FOR THE THREAD AND CODE
if __name__ == "__main__":
    #myelm = ELM327('/dev/ttyUSB0')
    myelm = ELM327('/dev/tty.SLAB_USBtoUART')
    myelm.open()
    atcnt, csts = myelm.connect()
    print atcnt, csts

    print myelm.getprotocol()
    print myelm.getprotocollongname()


    for i in xrange(10):
        myelm.command("ATDP")
        data, vld = myelm.response()
        print i, data, vld
        myelm.command("ATDPN")
        data, vld = myelm.response()
        print i, data, vld

    #cmdlist = [{'sid':0x01,'pid':0x0C},{'sid':0x01,'pid':0x0D}]
    #myelm.setuplooprun(cmdlist,0.1,0.5)
    #myelm.start()

    #dead = False
    #while not dead:
    #    try:
    #        for i in xrange(len(myelm.obdrespqueuelist)):
    #            tmp = myelm.obdrespqueuelist[i].get()
    #            print "%s\t%6.2f" % (tmp['name'],tmp['response'])
    #        print "----"
    #    except KeyboardInterrupt:
    #        dead = True

    myelm.kill()

