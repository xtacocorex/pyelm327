#!/usr/bin/env python

"""
ELM-327 - DTC TEST SCRIPT

"""

# UNIT TEST
if __name__ == "__main__":

    import sys,time
    import elm327
    try:
        import serial
    except ModuleError:
        sys.stderr.write("pyserial is needed")
        sys.exit()

    # OS X
    #PORT = "/dev/tty.SLAB_USBtoUART"
    
    # UBUNTU OR DEBIAN DERIVATIVE
    PORT = "/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0"
    
    # DEFAULT BAUD RATE
    BAUD = 38400

    myelm = elm327.ELM327(PORT,BAUD,units="imperial")
    # OPEN
    myelm.open()
    
    # TEST AUTO PROTOCOL
    print "SETTING AUTO PROTOCOL ONLY"
    myelm.setprotocolonlyauto()

    # TESTING CONNECT TO VEHICLE
    print "CONNECTING TO VEHICLE"
    myelm.connect(numretries=5)

    # GET PROTOCOL
    print "GETTING PROTOCOL AND NUMBER"
    tmp = myelm.getprotocol()
    print tmp
    tmp = myelm.getprotocolnumber()
    print tmp
    
    # GET THE DTC CODES
    print "\nGETTING THE DTC CODE RESPONSE"
    tmp = myelm.sendobd(0x03) 
    print tmp
    
    # CLOSE THE DEVICE
    myelm.close()

    # TEST END
    raw_input("ENTER TO CONTINUE")