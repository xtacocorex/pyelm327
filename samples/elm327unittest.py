#!/usr/bin/env python

"""
ELM-327 DEVICE UNIT TEST SCRIPT

"""

# UNIT TEST
if __name__ == "__main__":

    import sys,time
    import elm327
    try:
        import serial
    except ModuleError:
        sys.stderr.write("pyserial is needed")
        sys.exit()

    # OS X
    #PORT = "/dev/tty.SLAB_USBtoUART"
    
    # UBUNTU OR DEBIAN DERIVATIVE
    PORT = "/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0"
    
    # DEFAULT BAUD RATE
    BAUD = 38400

    myelm = elm327.ELM327(PORT,BAUD,units="imperial")
    # OPEN
    myelm.open()
    # GET DEVICE ID
    print "GETTING DEVICE IDENTITY"
    print myelm.identify()
    
    #print "SETTING LINEFEED MODE"
    #myelm.setlinefeed(False)
    
    # MULTI LINE FEED
    print "SENDING ATPPS TO GET DEVICE PROGRAMMABLE SETTINGS"
    print "THIS IS A TEST OF A MULTI-LINE RESPONSE"
    myelm.command("ATPPS")
    tmp, idt = myelm.response()
    print tmp, idt
    
    # TESTING BADNESS
    print "TESTING BADNESS TO SHOW HOW THE DEVICE RESPONDS"
    myelm.command("ATE3")
    tmp, idt = myelm.response()
    print tmp, idt

    # TEST AUTO PROTOCOL
    print "SETTING AUTO PROTOCOL ONLY"
    myelm.setprotocolonlyauto()

    # TESTING CONNECT TO VEHICLE
    print "CONNECTING TO VEHICLE"
    myelm.connect(numretries=5)

    # GET PROTOCOL
    print "GETTING PROTOCOL AND NUMBER"
    tmp = myelm.getprotocol()
    print tmp
    tmp = myelm.getprotocolnumber()
    print tmp

    # LETS SEE WHAT THE VEHICLE CAN PROVIDE
    # WE'RE ONLY GOING FROM 0x00 TO 0x4E AS 0x4E
    # IS THE HIGHEST THE CURRENT LIBRARY WILL SUPPORT
    print "\n===============\nFIGURING OUT WHAT THE VEHICLE SUPPORTS"
    pidlist = range(0x00,0x4E)
    for pid in pidlist:
        tmp = myelm.sendobd(0x01,pid)
        print "%02x%02x\t%15s\t%15s\t%10s" % (tmp["sid"],tmp["pid"],tmp["name"],tmp["response"],tmp["unit"])

    myelm.setheaderdisplay(False)
    print "\n===============\nRUNNING LOOP TO GET DATA"
    for i in xrange(15):
        tmp = myelm.sendobd(0x01,0x0C)  # RPM
        print tmp["response"],tmp["unit"]
        tmp = myelm.sendobd(0x01,0x0D)  # SPEED
        print tmp["response"],tmp["unit"]
        print "------------"
        time.sleep(0.5)

    # TEST GETTING THE VIN
    #tmp = myelm.sendobd(0x09,0x02)
    #print tmp

    # CLOSE THE DEVICE
    myelm.close()

    # TEST END
    raw_input("ENTER TO CONTINUE")
