This package provides a means to control ELM-327 based devices (anything that supports the AT command set) using python.

For installation:
sudo python setup.py install

This has been tested on an Intel Mac running 10.6.X and on a Toshiba running XUbuntu 12.04.

